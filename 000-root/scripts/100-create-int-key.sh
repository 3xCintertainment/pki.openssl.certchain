#! /bin/bash

source $PWD/scripts/000-vars.sh
source ../001-intermediate/scripts/000-vars.sh

read -p "Password: [Default: None]" password
if [ -n "$password" ]; then
  SSL_OPTS="-aes256 "
fi

read -p "Domain Name: [Default: None]" dName
if [ -n "$dName" ]; then
  SSL_OPTS="${SSL_OPTS} -out $INTCADIR/private/$dName.key.pem "
fi

read -p "Byte Count: [Default: 2048]" byteCount
if [ -n "$byteCount" ]; then
  SSL_OPTS="${SSL_OPTS} $bytecount"
else
  SSL_OPTS="${SSL_OPTS} 2048"
fi

openssl genrsa $SSL_OPTS

chmod 400 $INTCADIR/private/$dName.key.pem

source $PWD/scripts/101-create-cert.sh $dName

