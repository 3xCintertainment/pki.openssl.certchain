#! /bin/bash

openssl req -config $ROOTCADIR/openssl.cnf \
      -key $ROOTCADIR/private/ca.key.pem \
      -new -x509 -days 7300 -sha256 -extensions v3_ca \
      -out $ROOTCADIR/certs/ca.cert.pem

chmod 444 $ROOTCADIR/certs/ca.cert.pem
cp $ROOTCADIR/certs/ca.cert.pem $ROOTCADIR/certs/ca.crt

# verify root certificate
openssl x509 -noout -text -in certs/ca.cert.pem
