#! /bin/bash

source $PWD/scripts/000-vars.sh

source ../001-intermediate/scripts/000-vars.sh

openssl ca -config $ROOTCADIR/openssl.cnf -extensions v3_intermediate_ca \
      -days 3650 -notext -md sha256 \
      -in $INTCADIR/csr/intermediate.csr.pem \
      -out $INTCADIR/certs/intermediate.cert.pem

chmod 444 $INTCADIR/certs/intermediate.cert.pem

openssl x509 -noout -text \
      -in $INTCADIR/certs/intermediate.cert.pem
	  
	 
# create cert chain

cat $INTCADIR/certs/intermediate.cert.pem \
      $ROOTCADIR/certs/ca.cert.pem > $INTCADIR/certs/ca-chain.cert.pem
	  
chmod 444 $INTCADIR/certs/ca-chain.cert.pem
