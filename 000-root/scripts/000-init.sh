#! /bin/bash

source $PWD/scripts/000-vars.sh

mkdir certs crl newcerts private
chmod 700 private
touch index.txt
echo 1000 > serial

source $ROOTSCRIPTSDIR/001-create-root.sh
source $ROOTSCRIPTSDIR/002-create-root-cert.sh

echo 'Finished.'