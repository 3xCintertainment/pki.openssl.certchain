#! /bin/bash

source $PWD/scripts/000-vars.sh
source ../001-intermediate/scripts/000-vars.sh

SSL_OPTS=""

openssl req -config $INTCADIR/openssl.cnf \
		-key $INTCADIR/private/$1.key.pem \
		-new -sha256 -out $INTCADIR/csr/$1.csr.pem
		

read -p "CertType: user , server [Default: server]" certType
if [ -n "$certType" ]; then
  SSL_OPTS="${SSL_OPTS} -extensions ${certType}_cert "
fi
		
openssl ca -config $INTCADIR/openssl.cnf \
		-extensions ${SSL_OPTS} -days 375 -notext -md sha256 \
		-in $INTCADIR/csr/$dName.csr.pem \
		-out $INTCADIR/certs/$dName.cert.pem
		
chmod 444 $INTCADIR/certs/$dName.cert.pem
