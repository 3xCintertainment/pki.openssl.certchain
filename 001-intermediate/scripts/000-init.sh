#! /bin/bash

source $PWD/scripts/000-vars.sh

mkdir certs crl csr newcerts private
chmod 700 private
touch index.txt
echo 1000 > serial
echo 1000 > crlnumber

source $INTSCRIPTSDIR/001-create-key.sh
source $INTSCRIPTSDIR/002-create-csr.sh
echo 'Finished.'
echo 'CSR Generated and ready to be signed by the eye of god.'
