#! /bin/bash

source $PWD/scripts/000-vars.sh

openssl x509 -noout -text \
      -in $INTCADIR/certs/intermediate.cert.pem
