#! /bin/bash

openssl req -config $INTCADIR/openssl.cnf -new -sha256 \
      -key $INTCADIR/private/intermediate.key.pem \
      -out $INTCADIR/csr/intermediate.csr.pem

chmod 444 $INTCADIR/csr/intermediate.csr.pem

# doesnt work
# verify csr certificate
# openssl x509 -noout -text -in $INTCADIR/csr/intermediate.csr.pem

